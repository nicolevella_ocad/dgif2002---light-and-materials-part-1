
/*
  Project:      Light & Materials (part 3)
  Student:      Nicole Vella
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   Jan 30, 2020
  Based on:     Blink and Fade Assignment, Nicole Vella
*/

// Constant Value Variables - Using a const variable when the value won't change reduces overhead
const int redLed    = 11;  // RGB single LED
const int greenLed  = 10;
const int blueLed   = 9;

const int redLed2    = 6; // RGB single LED (because two leds is always better than one)
const int greenLed2  = 5;
const int blueLed2   = 3;

int tapSensor = 7;     // Pin the Tap Sensor is attached to.
int tapState = 0;         // The current state of the tap sensor.
int lastTapState = 0;     // The previous state of the tap sensor from the prior loop.

int randomRed = 0;        // Random red led brightness value.
int randomGreen = 0;      // Random green led brightness value.
int randomBlue = 0;       // Random blue led brightness value.

const int delayTime = 10; // the speed of the cycle/fade


void setup() {
  Serial.begin(9600);
  
  // Code here runs once when the board is powered on.
  pinMode(tapSensor, INPUT); // assign the tap sensor pin as INPUT
  pinMode(redLed, OUTPUT); // assign the variables to their pin OUTPUT
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  pinMode(redLed2, OUTPUT);
  pinMode(greenLed2, OUTPUT);
  pinMode(blueLed2, OUTPUT);

}

void loop() {

  colorCycle(); // A function that cycles through the colour spectrum.

//  checkTap();



}



void checkTap() {
  // Get current status of tap sensor, and set it as tapState.
  tapState = digitalRead(tapSensor);

  // Compare the tapState to lastTapState from the prior loop.
  // If the values are not equal, a tap happened, and we run a custom function.
  if (tapState != lastTapState) {
    pauseColor();
  }

  // Set lastTapState as tapState.
  // This allows us to detect when it changes after the loop.
  lastTapState = tapState;
}

void randomColor() {
  // A function that generates three random numbers.
  // Each number will be set to the brightness value for the R, G, or B Led.

  // Generate random numbers between 1 and 255 using the random() function
  randomRed = random(1, 255);
  randomGreen = random(1, 255);
  randomBlue = random(1, 255);

  // Change the LEDs brightness to the random values set above.
  analogWrite(redLed, randomRed);
  analogWrite(greenLed, randomGreen);
  analogWrite(blueLed,  randomBlue);
  analogWrite(redLed2, randomRed);
  analogWrite(greenLed2, randomGreen);
  analogWrite(blueLed2,  randomBlue);

}


void colorCycle() {
  // A function that cycles through the colour spectrum in order of red -> green -> blue.
  // By overlapping the fades, we can mix the rgb values within the single LED and create
  // a full spectrum of colours.

  analogWrite(greenLed, 255); // we start with green on high so that the cycle loops seamlessly
  analogWrite(greenLed2, 255);

  checkTap();

  for (int i = 0; i <= 255; i++) {  // turn up red
      checkTap();
  analogWrite(redLed, i);
    analogWrite(redLed2, i);
    checkTap();
    delay(delayTime);
    checkTap();
  }

  for (int i = 255; i >= 0; i--) {  // turn down green
    checkTap();
    analogWrite(greenLed, i);
    analogWrite(greenLed2, i);
    checkTap();
    delay(delayTime);
    checkTap();
  }

  for (int i = 0; i <= 255; i++) { // turn up blue
    checkTap();
    analogWrite(blueLed, i);
    analogWrite(blueLed2, i);
    checkTap();
    delay(delayTime);
    checkTap();
  }

  for (int i = 255; i >= 0; i--) { // turn down red
    checkTap();
    analogWrite(redLed, i);
    analogWrite(redLed2, i);
    checkTap();
    delay(delayTime);
    checkTap();
  }

  for (int i = 0; i <= 255; i++) {  // turn up green
    checkTap();
    analogWrite(greenLed, i);
    analogWrite(greenLed2, i);
    checkTap();
    delay(delayTime);
    checkTap();
  }

  for (int i = 255; i >= 0; i--) { // turn down blue
    checkTap();
    analogWrite(blueLed, i);
    analogWrite(blueLed2, i);
    checkTap();
    delay(delayTime);
    checkTap();
  }

}

void pauseColor() {
    Serial.println("pause");

}