
/*
  Project:      Light & Materials
  Student:      Nicole Vella
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   Sept 30, 2019
  Based on:     Sample code for Tap Sensor, RGBFreak, https://www.instructables.com/id/Arduino-37-in-1-Sensors-Kit-Explained

*/

// inputs
int  tapSensor = 7;     // Pin the Tap Sensor is attached to.

// outputs
int redLed     = 11;      // RGB single LED.
int greenLed   = 10;
int blueLed    = 9;
int redLed2    = 6;       // RGB single LED (because two leds is always better than one).
int greenLed2  = 5;
int blueLed2   = 3;

// props
int delayTime = 10;       // the speed of the cycle/fade
int tapState = 0;         // The current state of the tap sensor.
int lastTapState = 0;     // The previous state of the tap sensor from the prior loop.
int randomRed = 0;        // Random red led brightness value.
int randomGreen = 0;      // Random green led brightness value.
int randomBlue = 0;       // Random blue led brightness value.

void setup() {
  // Code here runs once when the board is powered on.

  pinMode(tapSensor, INPUT); // assign the tap sensor pin as INPUT
  pinMode(redLed, OUTPUT);   // assign the leds to their pin OUTPUT
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  pinMode(redLed2, OUTPUT);
  pinMode(greenLed2, OUTPUT);
  pinMode(blueLed2, OUTPUT);


  analogWrite(redLed, 127);  // When cube is turned on, set leds to white.
  analogWrite(greenLed, 127);
  analogWrite(blueLed,  127);
  analogWrite(redLed2, 127);
  analogWrite(greenLed2, 127);
  analogWrite(blueLed2,  127);
}

void loop() {
  // Code here runs in a loop while the board is powered on.

  // Get current status of tap sensor, and set it as tapState.
  tapState = digitalRead(tapSensor);

  // Compare the tapState to lastTapState from the prior loop.
  // If the values are not equal, a tap happened, and we run a custom function.
  if (tapState != lastTapState) {
    randomColor();
  } 

  // Set lastTapState as tapState.
  // This allows us to detect when it changes after the loop.
  lastTapState = tapState;

}

void randomColor() {
  // A function that generates three random numbers.
  // Each number will be set to the brightness value for the R, G, or B Led.

  // Generate random numbers between 1 and 255 using the random() function
  randomRed = random(1, 255);
  randomGreen = random(1, 255);
  randomBlue = random(1, 255);

  // Change the LEDs brightness to the random values set above.
  analogWrite(redLed, randomRed);
  analogWrite(greenLed, randomGreen);
  analogWrite(blueLed,  randomBlue);
  analogWrite(redLed2, randomRed);
  analogWrite(greenLed2, randomGreen);
  analogWrite(blueLed2,  randomBlue);

}
