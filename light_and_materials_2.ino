
/*
  Project:      Light & Materials (part 2)
  Student:      Nicole Vella
  Course:       DGIF 2002 - Physical Computing - OCAD University
  Created on:   Sept 30, 2019
  Based on:     Blink and Fade Assignment, Nicole Vella
*/

// Constant Value Variables - Using a const variable when the value won't change reduces overhead
const int redLed    = 11;  // RGB single LED
const int greenLed  = 10;
const int blueLed   = 9;

const int redLed2    = 6; // RGB single LED (because two leds is always better than one)
const int greenLed2  = 5;
const int blueLed2   = 3;

const int delayTime = 10; // the speed of the cycle/fade


void setup() {
  // Code here runs once when the board is powered on.

  pinMode(redLed, OUTPUT); // assign the variables to their pin OUTPUT
  pinMode(greenLed, OUTPUT);
  pinMode(blueLed, OUTPUT);
  pinMode(redLed2, OUTPUT);
  pinMode(greenLed2, OUTPUT);
  pinMode(blueLed2, OUTPUT);

}

void loop() {
  // Code here runs in a loop while the board is powered on.

  colorCycle(); // A function that cycles through the colour spectrum.

}

void colorCycle() {
  // A function that cycles through the colour spectrum in order of red -> green -> blue.
  // By overlapping the fades, we can mix the rgb values within the single LED and create
  // a full spectrum of colours.

  analogWrite(greenLed, 255); // we start with green on high so that the cycle loops seamlessly
  analogWrite(greenLed2, 255);

  for (int i = 0; i <= 255; i++) {  // turn up red
    analogWrite(redLed, i);
    analogWrite(redLed2, i);
    delay(delayTime);
  }

  for (int i = 255; i >= 0; i--) {  // turn down green
    analogWrite(greenLed, i);
    analogWrite(greenLed2, i);
    delay(delayTime);
  }

  for (int i = 0; i <= 255; i++) { // turn up blue
    analogWrite(blueLed, i);
    analogWrite(blueLed2, i);
    delay(delayTime);
  }

  for (int i = 255; i >= 0; i--) { // turn down red
    analogWrite(redLed, i);
    analogWrite(redLed2, i);
    delay(delayTime);
  }

  for (int i = 0; i <= 255; i++) {  // turn up green
    analogWrite(greenLed, i);
    analogWrite(greenLed2, i);
    delay(delayTime);
  }

  for (int i = 255; i >= 0; i--) { // turn down blue
    analogWrite(blueLed, i);
    analogWrite(blueLed2, i);
    delay(delayTime);
  }

}